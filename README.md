# DIFI Map: Visualize Datasets available from [DIFI/Data.norge.no](https://data.norge.no/)

The idea is to use the [Data.norge.no's own API *"Data.norge-API-et"*](https://data.norge.no/data/direktoratet-forvaltning-og-ikt/datanorgeno-nasjonal-katalog-over-åpne-offentlige-datasett) in order to build a visual map to guide the curious developer.

## API documentation

[https://data.norge.no/api-0](https://data.norge.no/api-0)


Let's play with [jq](https://stedolan.github.io/jq/manual).

You can export the full dataset in one command:

```shell
curl 'https://data.norge.no/api/dcat/data.json?page=1' | jq '.' > difi.json
```

or you can explore the data like so:
```shell
curl 'https://data.norge.no/api/dcat/data.json?page=1' | jq 'keys' 
```

```javascript
[
  "datasets",
  "homepage",
  "title"
]
```

Get the main keys and their data type:

```shell
curl 'https://data.norge.no/api/dcat/data.json?page=1' | jq 'keys_unsorted, map(type)'
```

```javascript
[
  "title",
  "homepage",
  "datasets"
]
[
  "string",
  "string",
  "array"
]

```

Get the full data about the first dataset:

```shell
curl 'https://data.norge.no/api/dcat/data.json?page=1' | jq '.datasets' | jq '.[0]'
```

```javascript
{
  "id": "https://data.norge.no/node/1484",
  "title": "Kolumbus Real Time Open Data ",
  "description": [
    {
      "language": "nb",
      "value": "<p>Kolumbus Real Time Open Data is standardized on SIRI&nbsp; (Service Interface for Real Time Information) which is a web based XML&nbsp;protocol to allow distributed computers to exchange&nbsp;real time&nbsp;information about&nbsp;public transport&nbsp;services and vehicles.&nbsp;</p><p>SIRI is a&nbsp;CEN&nbsp;technical specification, and is based on the&nbsp;Transmodel&nbsp;abstract model for public transport information, and comprises a general purpose model, and an&nbsp;XML&nbsp;schema for public transport information.</p><p>For more information, see https://en.wikipedia.org/wiki/Service_Interface_for_Real_Time_Information</p><p>Kolumbus has implemented three SIRI interfaces:</p><p>SIRI VM &ndash; Vehicle Monitoring (positioning and status for all buses)<br />SIRI SM &ndash; Stop Monitoring (departure prognosis for individual bus stops)<br />SIRI SX &ndash; Situation Exchange (deviation messages)</p><p>Kolumbus currently support the SIRI version 1.4.&nbsp;</p><p>There are approximately 400 buses that operate and have real-time information&nbsp;in Rogaland.</p><p>For technical developer details and examples, see&nbsp;https://www.kolumbus.no/siri/</p>"
    }
  ],
  "landingPage": "https://www.kolumbus.no/siri",
  "issued": "2015-11-25",
  "modified": "2017-02-27T14:40",
  "language": [
    "ENG"
  ],
  "publisher": {
    "name": "Kolumbus AS",
    "mbox": "sanntid@kolumbus.no"
  },
  "keyword": [
    "Transport"
  ]
}

```

Find out how many are they and their titles:

```shell
curl 'https://data.norge.no/api/dcat/data.json?page=1' | jq '.datasets' | jq 'length, map(.title)'
```

```javascript
100
[
  "Kolumbus Real Time Open Data ",
  "Skoler og klasser som har meldt seg på kodetimen",
  "Produksjonstilskudd jordbruksforetak - søknadsomgang august 2016",
  "Ruter kollektivtrafikkdata",
  "Revidert statsbudsjett 2016",
  "Statsbudsjettet 2016",
  "ATC indeks for legemidler",
  "Oslo kommune - Åpne data i Statistikkbanken",
  "Prognose for luftkvalitet Stavanger",
  "Stavanger Parkering",
  "Luftmåling Stavanger",
  "Fotgjengerdata Stavanger",
  "Sykkeldata",
  "Lokalisering luftmålere Stavanger",
  "Nasjonale faglige retningslinjer, veiledere og pakkeforløp",
  "Minibanker i Stavanger",
  "Lokalisering sykkeltellere Stavanger",
  "Sykkeltelling 2012-2016",
  "Kommune- og fylkesstruktur",
  "Fotgjengertelling 2015-2016",
  "Enhetsregisteret",
  "Frivillighetsregisteret",
  "Underenheter i Enhetsregisteret",
  "Registeret for offentlig støtte",
  "Partiregisteret",
  "Sektorkoder i Enhetsregisteret",
  "Organisasjonsformer i Enhetsregisteret",
  "Næringskoder i Enhetsregisteret",
  "Statsbudsjettet 2017",
  "Land og landkoder",
  "Grunnkretser og delområder",
  "Matvaretabellen",
  "Kommuneinndeling",
  "Produksjons- og avløsertilskudd jordbruksforetak - søknadsomgang januar 2016",
  "API mot Statistikkbanken til SSB",
  "Om innvandring til og integrering i Norge",
  "Statsregnskapet",
  "Sykkelruter Stavanger",
  "Skjenkesteder i Stavanger kommune",
  "Lekeplasser Stavanger kommune utstyr",
  "Lekeplasser Stavanger kommune adresse",
  "Investeringsregnskap 2. tertial 2016",
  "Investeringsbudsjett 2017-2020 forslag Stavanger kommune",
  "Driftsregnskap 2. tertial 2016",
  "Driftsbudsjett 2017-2020 forslag Stavanger kommune",
  "Anleggsregister",
  "Flomvarsler fra NVE",
  "Snøskredvarsler fra NVE",
  "Kvalitet på nett - resultatliste 2016",
  "Kvalitet på nett - detaljresultat 2016",
  "Økonomiplan 2013-2016",
  "Tidstyver i forvaltningen",
  "Telefonnummer til statlige virksomheter og kommuner",
  "Postjournalstatistikk (OEP) - Innsynskrav 2011",
  "Postjournalstatistikk (OEP) - Innsynskrav 2010",
  "Postadresser til statlige virksomheter og kommuner",
  "Norske mottakere i OpenPEPPOL",
  "Norge.no - tjenesteeiere. Statlige virksomheter, kommuner og fylkeskommuner.",
  "Norge.no - digitale tjenester fra det offentlige",
  "Nettadresser (url) til statlige virksomheter og kommuner",
  "Kvalitet på nett - resultatliste 2013",
  "Kvalitet på nett - resultatliste 2011",
  "Kvalitet på nett - resultatliste 2010",
  "Kvalitet på nett - detaljresultat 2013",
  "Kvalitet på nett - detaljresultat 2011",
  "Kvalitet på nett - detaljresultat 2010",
  "Kvalitet på digitale tjenester - resultatliste 2014",
  "Kvalitet på digitale tjenester - detaljresultat 2014",
  "Kartkoordinater til statlige virksomheter og kommuner",
  "Flyreiseaktivitet i staten 2009-2013",
  "Enhetene i offentlig forvaltning",
  "Data over hvilke kommuner regionale statlige virksomheter dekker ",
  "Besøksadresse til statlige virksomheter og kommuner",
  "Statens satser - Utland",
  "Fylkesinndeling",
  "Standard for yrkesklassifisering (STYRK08)",
  "Bydelsinndeling",
  "Kartdata N2000",
  "Kartdata N2000",
  "Brannstasjoner i Norge",
  "Statistikk fra tilskuddsordningen organisert beitebruk (drift av beitelag)",
  "Klassifikasjoner og kodelister (KLASS)",
  "API Ruteplantjeneste for bil",
  "Trafikkinformasjon på DATEX-format",
  "Norge Rundt Statistikkmoro",
  "Offentlige data via BarentsWatch",
  "Produksjons- og avløsertilskudd jordbruksforetak - søknadsomgang januar 2015",
  "Los - felles terminologi for offentlige tjenester ",
  "Kulturminnebilder",
  " Nasjonale rutedata for Norge",
  "Bestemmelsesnøkler",
  "Arter på nett",
  "Norsk rødliste for arter",
  "Fremmede arter i Norge",
  "Naturtypebasen",
  "Kulturminnesøk",
  "FEST (Forskrivnings- og ekspedisjonsstøtte)",
  "Norsk bistand i tall - kart-API",
  "Norsk bistand i tall - visning pr sektor",
  "Statistikk over norsk offisiell bistand i tall"
]
```


Get short infos (title, keywords, issued date, language) about each *datasets*:

```shell
curl 'https://data.norge.no/api/dcat/data.json?page=1' | jq '.datasets' | jq '.[]' | jq -c '{(.title): .keyword}'
```

```javascript
{"Kolumbus Real Time Open Data ":["Transport"]}
{"Skoler og klasser som har meldt seg på kodetimen":["Utdanning, kultur og sport"]}
{"Produksjonstilskudd jordbruksforetak - søknadsomgang august 2016":["Forvaltning og offentlig sektor","Jordbruk, fiskeri, skogbruk og mat"]}
{"Ruter kollektivtrafikkdata":["Transport"]}
{"Revidert statsbudsjett 2016":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Statsbudsjettet 2016":["Forvaltning og offentlig sektor"]}
{"ATC indeks for legemidler":["Helse"]}
{"Oslo kommune - Åpne data i Statistikkbanken":["Befolkning og samfunn","Forvaltning og offentlig sektor","Miljø"]}
{"Prognose for luftkvalitet Stavanger":["Forvaltning og offentlig sektor","Miljø"]}
{"Stavanger Parkering":["Forvaltning og offentlig sektor","Transport"]}
{"Luftmåling Stavanger":["Forvaltning og offentlig sektor","Miljø"]}
{"Fotgjengerdata Stavanger":["Forvaltning og offentlig sektor","Utdanning, kultur og sport"]}
{"Sykkeldata":["Forvaltning og offentlig sektor","Miljø","Utdanning, kultur og sport"]}
{"Lokalisering luftmålere Stavanger":["Forvaltning og offentlig sektor","Helse","Miljø"]}
{"Nasjonale faglige retningslinjer, veiledere og pakkeforløp":["Forvaltning og offentlig sektor","Helse"]}
{"Minibanker i Stavanger":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Lokalisering sykkeltellere Stavanger":["Forvaltning og offentlig sektor","Utdanning, kultur og sport"]}
{"Sykkeltelling 2012-2016":["Forvaltning og offentlig sektor"]}
{"Kommune- og fylkesstruktur":["Forvaltning og offentlig sektor"]}
{"Fotgjengertelling 2015-2016":["Forvaltning og offentlig sektor"]}
{"Enhetsregisteret":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Frivillighetsregisteret":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Underenheter i Enhetsregisteret":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Registeret for offentlig støtte":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Partiregisteret":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Sektorkoder i Enhetsregisteret":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Organisasjonsformer i Enhetsregisteret":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Næringskoder i Enhetsregisteret":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Statsbudsjettet 2017":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Land og landkoder":["Forvaltning og offentlig sektor","Internasjonale temaer"]}
{"Grunnkretser og delområder":["Forvaltning og offentlig sektor","Regioner og byer"]}
{"Matvaretabellen":["Befolkning og samfunn","Helse","Jordbruk, fiskeri, skogbruk og mat"]}
{"Kommuneinndeling":["Forvaltning og offentlig sektor","Regioner og byer"]}
{"Produksjons- og avløsertilskudd jordbruksforetak - søknadsomgang januar 2016":["Forvaltning og offentlig sektor","Jordbruk, fiskeri, skogbruk og mat"]}
{"API mot Statistikkbanken til SSB":["Befolkning og samfunn","Energi","Forvaltning og offentlig sektor","Helse","Internasjonale temaer","Jordbruk, fiskeri, skogbruk og mat","Justis, rettssystem og allmenn sikkerhet","Miljø","Regioner og byer","Transport","Utdanning, kultur og sport","Vitenskap og teknologi","Økonomi og finans"]}
{"Om innvandring til og integrering i Norge":["Befolkning og samfunn","Utdanning, kultur og sport"]}
{"Statsregnskapet":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Sykkelruter Stavanger":["Forvaltning og offentlig sektor","Transport","Utdanning, kultur og sport"]}
{"Skjenkesteder i Stavanger kommune":["Forvaltning og offentlig sektor"]}
{"Lekeplasser Stavanger kommune utstyr":["Forvaltning og offentlig sektor","Miljø","Utdanning, kultur og sport"]}
{"Lekeplasser Stavanger kommune adresse":["Befolkning og samfunn","Forvaltning og offentlig sektor","Miljø","Utdanning, kultur og sport"]}
{"Investeringsregnskap 2. tertial 2016":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Investeringsbudsjett 2017-2020 forslag Stavanger kommune":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Driftsregnskap 2. tertial 2016":["Forvaltning og offentlig sektor","Økonomi og finans"]}
{"Driftsbudsjett 2017-2020 forslag Stavanger kommune":["Forvaltning og offentlig sektor","Regioner og byer","Økonomi og finans"]}
{"Anleggsregister":["Befolkning og samfunn","Forvaltning og offentlig sektor","Regioner og byer"]}
{"Flomvarsler fra NVE":["Miljø"]}
{"Snøskredvarsler fra NVE":["Befolkning og samfunn","Helse","Utdanning, kultur og sport"]}
{"Kvalitet på nett - resultatliste 2016":["Forvaltning og offentlig sektor"]}
{"Kvalitet på nett - detaljresultat 2016":["Forvaltning og offentlig sektor"]}
{"Økonomiplan 2013-2016":["Økonomi og finans"]}
{"Tidstyver i forvaltningen":["Forvaltning og offentlig sektor"]}
{"Telefonnummer til statlige virksomheter og kommuner":["Forvaltning og offentlig sektor","Regioner og byer"]}
{"Postjournalstatistikk (OEP) - Innsynskrav 2011":["Forvaltning og offentlig sektor"]}
{"Postjournalstatistikk (OEP) - Innsynskrav 2010":["Forvaltning og offentlig sektor"]}
{"Postadresser til statlige virksomheter og kommuner":["Forvaltning og offentlig sektor","Regioner og byer"]}
{"Norske mottakere i OpenPEPPOL":["Forvaltning og offentlig sektor","Transport","Økonomi og finans"]}
{"Norge.no - tjenesteeiere. Statlige virksomheter, kommuner og fylkeskommuner.":["Forvaltning og offentlig sektor"]}
{"Norge.no - digitale tjenester fra det offentlige":["Forvaltning og offentlig sektor"]}
{"Nettadresser (url) til statlige virksomheter og kommuner":["Forvaltning og offentlig sektor","Regioner og byer"]}
{"Kvalitet på nett - resultatliste 2013":["Forvaltning og offentlig sektor"]}
{"Kvalitet på nett - resultatliste 2011":["Forvaltning og offentlig sektor"]}
{"Kvalitet på nett - resultatliste 2010":["Forvaltning og offentlig sektor"]}
{"Kvalitet på nett - detaljresultat 2013":["Forvaltning og offentlig sektor"]}
{"Kvalitet på nett - detaljresultat 2011":["Forvaltning og offentlig sektor"]}
{"Kvalitet på nett - detaljresultat 2010":["Forvaltning og offentlig sektor"]}
{"Kvalitet på digitale tjenester - resultatliste 2014":["Forvaltning og offentlig sektor"]}
{"Kvalitet på digitale tjenester - detaljresultat 2014":["Forvaltning og offentlig sektor"]}
{"Kartkoordinater til statlige virksomheter og kommuner":["Forvaltning og offentlig sektor","Regioner og byer"]}
{"Flyreiseaktivitet i staten 2009-2013":["Forvaltning og offentlig sektor","Transport"]}
{"Enhetene i offentlig forvaltning":["Forvaltning og offentlig sektor","Regioner og byer"]}
{"Data over hvilke kommuner regionale statlige virksomheter dekker ":["Forvaltning og offentlig sektor","Regioner og byer"]}
{"Besøksadresse til statlige virksomheter og kommuner":["Forvaltning og offentlig sektor","Regioner og byer"]}
{"Statens satser - Utland":["Forvaltning og offentlig sektor","Transport"]}
{"Fylkesinndeling":["Forvaltning og offentlig sektor","Regioner og byer"]}
{"Standard for yrkesklassifisering (STYRK08)":["Befolkning og samfunn","Forvaltning og offentlig sektor"]}
{"Bydelsinndeling":["Forvaltning og offentlig sektor","Regioner og byer"]}
{"Kartdata N2000":["Forvaltning og offentlig sektor"]}
{"Kartdata N2000":["Forvaltning og offentlig sektor"]}
{"Brannstasjoner i Norge":["Befolkning og samfunn","Forvaltning og offentlig sektor","Justis, rettssystem og allmenn sikkerhet"]}
{"Statistikk fra tilskuddsordningen organisert beitebruk (drift av beitelag)":["Forvaltning og offentlig sektor","Jordbruk, fiskeri, skogbruk og mat","Miljø"]}
{"Klassifikasjoner og kodelister (KLASS)":["Befolkning og samfunn","Forvaltning og offentlig sektor","Helse","Internasjonale temaer","Jordbruk, fiskeri, skogbruk og mat","Regioner og byer","Transport","Utdanning, kultur og sport","Økonomi og finans"]}
{"API Ruteplantjeneste for bil":["Transport"]}
{"Trafikkinformasjon på DATEX-format":["Transport"]}
{"Norge Rundt Statistikkmoro":["Utdanning, kultur og sport"]}
{"Offentlige data via BarentsWatch":["Forvaltning og offentlig sektor","Jordbruk, fiskeri, skogbruk og mat","Transport"]}
{"Produksjons- og avløsertilskudd jordbruksforetak - søknadsomgang januar 2015":["Forvaltning og offentlig sektor","Jordbruk, fiskeri, skogbruk og mat"]}
{"Los - felles terminologi for offentlige tjenester ":["Forvaltning og offentlig sektor"]}
{"Kulturminnebilder":["Miljø"]}
{" Nasjonale rutedata for Norge":["Befolkning og samfunn","Miljø","Transport"]}
{"Bestemmelsesnøkler":["Miljø"]}
{"Arter på nett":["Miljø"]}
{"Norsk rødliste for arter":["Miljø"]}
{"Fremmede arter i Norge":["Miljø"]}
{"Naturtypebasen":["Miljø"]}
{"Kulturminnesøk":["Forvaltning og offentlig sektor","Justis, rettssystem og allmenn sikkerhet","Miljø","Regioner og byer","Utdanning, kultur og sport"]}
{"FEST (Forskrivnings- og ekspedisjonsstøtte)":["Helse"]}
{"Norsk bistand i tall - kart-API":["Internasjonale temaer"]}
{"Norsk bistand i tall - visning pr sektor":["Internasjonale temaer"]}
{"Statistikk over norsk offisiell bistand i tall":["Internasjonale temaer"]}
```


Filter the data to find every datasets provided by a given publisher:

```shell
curl 'https://data.norge.no/api/dcat/data.json?page=1' | jq '.datasets'  | jq  'map(select(has("keyword")))' | jq 'map(select(.publisher.name=="Kolumbus AS"))'
```

```javascript
[
  {
    "id": "https://data.norge.no/node/1484",
    "title": "Kolumbus Real Time Open Data ",
    "description": [
      {
        "language": "nb",
        "value": "<p>Kolumbus Real Time Open Data is standardized on SIRI&nbsp; (Service Interface for Real Time Information) which is a web based XML&nbsp;protocol to allow distributed computers to exchange&nbsp;real time&nbsp;information about&nbsp;public transport&nbsp;services and vehicles.&nbsp;</p><p>SIRI is a&nbsp;CEN&nbsp;technical specification, and is based on the&nbsp;Transmodel&nbsp;abstract model for public transport information, and comprises a general purpose model, and an&nbsp;XML&nbsp;schema for public transport information.</p><p>For more information, see https://en.wikipedia.org/wiki/Service_Interface_for_Real_Time_Information</p><p>Kolumbus has implemented three SIRI interfaces:</p><p>SIRI VM &ndash; Vehicle Monitoring (positioning and status for all buses)<br />SIRI SM &ndash; Stop Monitoring (departure prognosis for individual bus stops)<br />SIRI SX &ndash; Situation Exchange (deviation messages)</p><p>Kolumbus currently support the SIRI version 1.4.&nbsp;</p><p>There are approximately 400 buses that operate and have real-time information&nbsp;in Rogaland.</p><p>For technical developer details and examples, see&nbsp;https://www.kolumbus.no/siri/</p>"
      }
    ],
    "landingPage": "https://www.kolumbus.no/siri",
    "issued": "2015-11-25",
    "modified": "2017-02-27T14:40",
    "language": [
      "ENG"
    ],
    "publisher": {
      "name": "Kolumbus AS",
      "mbox": "sanntid@kolumbus.no"
    },
    "keyword": [
      "Transport"
    ]
  }
]
```
 

Display all subject that contains *Transport* (used as keyword):

```shell
curl 'https://data.norge.no/api/dcat/data.json?page=1' | jq '.datasets'  | jq  'map(select(has("keyword")))' | jq '.[].keyword | tostring | select(contains("Transport"))'

```

```javascript
"[\"Transport\"]"
"[\"Transport\"]"
"[\"Forvaltning og offentlig sektor\",\"Transport\"]"
"[\"Befolkning og samfunn\",\"Energi\",\"Forvaltning og offentlig sektor\",\"Helse\",\"Internasjonale temaer\",\"Jordbruk, fiskeri, skogbruk og mat\",\"Justis, rettssystem og allmenn sikkerhet\",\"Miljø\",\"Regioner og byer\",\"Transport\",\"Utdanning, kultur og sport\",\"Vitenskap og teknologi\",\"Økonomi og finans\"]"
"[\"Forvaltning og offentlig sektor\",\"Transport\",\"Utdanning, kultur og sport\"]"
"[\"Forvaltning og offentlig sektor\",\"Transport\",\"Økonomi og finans\"]"
"[\"Forvaltning og offentlig sektor\",\"Transport\"]"
"[\"Forvaltning og offentlig sektor\",\"Transport\"]"
"[\"Befolkning og samfunn\",\"Forvaltning og offentlig sektor\",\"Helse\",\"Internasjonale temaer\",\"Jordbruk, fiskeri, skogbruk og mat\",\"Regioner og byer\",\"Transport\",\"Utdanning, kultur og sport\",\"Økonomi og finans\"]"
"[\"Transport\"]"
"[\"Transport\"]"
"[\"Forvaltning og offentlig sektor\",\"Jordbruk, fiskeri, skogbruk og mat\",\"Transport\"]"
"[\"Befolkning og samfunn\",\"Miljø\",\"Transport\"]"

```


